package co.simplon.library.controllers;

import co.simplon.library.models.Book;
import co.simplon.library.services.BookService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController // inclut @ResponseBody
public class BookController {

    private final BookService bookService;

    // injection de dependance par constructeur
    public BookController(BookService bookServiceInjected) {
        this.bookService = bookServiceInjected;
    }

    @GetMapping("/books")
    public List<Book> getBooks(){
      return bookService.getBooks();
    }

    @GetMapping("/books/{id}")
    public Book getBook(@PathVariable Long id){ // @PathVariable indique à Spring que la valeur {id} dans le chemin de l'URL doit être passée à la méthode getBook comme argument.
        return bookService.getBook(id);
    }

    @GetMapping("/books/title/{title}")
    public List<Book> getBookByTitle(@PathVariable String title){
        return bookService.getBookByTitle(title);
    }

    @PostMapping("/books")
    public Book addBook(@RequestBody Book book){ // @RequestBody indique que le paramètre book doit être lié au corps de la requête HTTP
        return bookService.saveBook(book);
    }

    @PutMapping("/books/{id}")
    public Book updateBook(@RequestBody Book book, @PathVariable Long id){
       return bookService.updateBook(book, id);
    }

    @DeleteMapping("/books/{id}")
    public void deleteBook(@PathVariable Long id){
        bookService.deleteBook(id);
    }

}
