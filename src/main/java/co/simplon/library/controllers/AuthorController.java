package co.simplon.library.controllers;

import co.simplon.library.models.Author;
import co.simplon.library.models.Book;
import co.simplon.library.services.AuthorService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AuthorController {

    private AuthorService authorService;

    public AuthorController(AuthorService authorServiceInjected){
        this.authorService = authorServiceInjected;
    }

    @GetMapping("/authors/{id}")
    public Author getAuthor(@PathVariable Long id){
        return authorService.getAuthor(id);
    }
    @GetMapping("/authors")
    public List<Author> getAuthors(){
        return authorService.getAuthors();
    }
    @DeleteMapping("/authors/{id}")
    public void deleteAuthor(@PathVariable Long id){
        authorService.deleteAuthor(id);
    }
    @GetMapping("/authors/{id}/books")
    public List<Book> getBooksByAuthor(@PathVariable Long id) {
        Author author = authorService.getAuthorWithBooks(id);
        return author.getBooks();
    }

}
