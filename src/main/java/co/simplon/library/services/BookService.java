package co.simplon.library.services;

import co.simplon.library.models.Author;
import co.simplon.library.models.Book;
import co.simplon.library.models.Category;
import co.simplon.library.repository.BookRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class BookService {

    private BookRepository bookRepository;
    private CategoryService categoryService;
    private AuthorService authorService;

    public BookService(BookRepository bookRepositoryInjected, CategoryService categoryServiceInjected, AuthorService authorServiceInjected){
        this.bookRepository = bookRepositoryInjected;
        this.categoryService = categoryServiceInjected;
        this.authorService = authorServiceInjected;
    }

    public List<Book> getBooks() {
        return bookRepository.findAll();
    }

    public Book getBook(Long id) {
        return bookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Aucun livre avec ID " + id));
    }

    public List<Book> getBookByTitle(String title) {
        return bookRepository.findByTitleContainingIgnoreCase(title);
    }

    public Book saveBook(@RequestBody Book book){

        // save category
        if(book.getCategory() != null) {
           Category savedCategory = categoryService.saveCategory(book.getCategory());
           book.setCategory(savedCategory);
        }

        // save author
        if (book.getAuthors() != null && !book.getAuthors().isEmpty()){
            Set<Author> savedAuthors = new HashSet<>();

            for(Author author : book.getAuthors()) {
                Author savedAuthor = authorService.saveAuthor(author);
                savedAuthors.add(savedAuthor);
            }
            book.setAuthors(savedAuthors);
        }

        return bookRepository.save(book);
    }

    public Book updateBook(Book bookWithUpdates, Long id) {
        Optional<Book> bookToUpdateOptional = bookRepository.findById(id); // findById() renvoie un Optional<Book> pour gérer les cas où 0 livre correspondant à l'id n'est trouvé
        if (bookToUpdateOptional.isPresent()) {
            Book bookToUpdate = bookToUpdateOptional.get(); // save() attend un objet de type Book
            bookToUpdate.setTitle(bookWithUpdates.getTitle());
            bookToUpdate.setDescription(bookWithUpdates.getDescription());
            bookToUpdate.setAvailable(bookWithUpdates.getAvailable());

            // update category
            Category updatedCategory = bookWithUpdates.getCategory();
            if (updatedCategory != null) {
                    Category categoryToSet = categoryService.saveCategory(updatedCategory);
                    bookToUpdate.setCategory(categoryToSet);
            }

            // update author
           Set<Author> updatedAuthors = bookWithUpdates.getAuthors();
           if (updatedAuthors != null && !updatedAuthors.isEmpty()) {

               Set<Author> authorsToSet = new HashSet<>();
               for (Author author : updatedAuthors){
                   Author savedAuthor = authorService.saveAuthor(author);
                   authorsToSet.add(savedAuthor);
               }
               bookToUpdate.setAuthors(authorsToSet);
           }
            return bookRepository.save(bookToUpdate);
        } else {
            throw new EntityNotFoundException("Aucun livre avec ID " + id);
        }
    }

    public void deleteBook(Long id){
        bookRepository.deleteById(id);
    }
}
