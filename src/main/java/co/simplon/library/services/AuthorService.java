package co.simplon.library.services;

import co.simplon.library.models.Author;
import co.simplon.library.repository.AuthorRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorService {

    private AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepositoryInjected){
        this.authorRepository = authorRepositoryInjected;
    }

    public Author getAuthor(Long id){
        return authorRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Aucun auteur avec ID " + id));
    }

    public List<Author> getAuthors(){
        return authorRepository.findAll();
    }

     public Author saveAuthor(@RequestBody Author author){
            Optional<Author> existingAuthor = authorRepository.findById(author.getAuthorId());
            if (!existingAuthor.isPresent()) {
               return authorRepository.save(author);
            } else {
               return existingAuthor.get();
            }
     };

    public void deleteAuthor(Long id){
        authorRepository.deleteById(id);
    }

    public Author getAuthorWithBooks(Long authorId) {
        return authorRepository.findById(authorId).orElseThrow(() -> new EntityNotFoundException("Author not found"));
    }
}
