package co.simplon.library.services;

import co.simplon.library.models.Category;
import co.simplon.library.repository.CategoryRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepositoryInjected) {
        this.categoryRepository = categoryRepositoryInjected;
    }

    public List<Category> getCategories() {
        return categoryRepository.findAll();
    }

    public Category getCategory(Long id){
        return categoryRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Aucune catégorie avec ID " + id));
    }

    public Category saveCategory(@RequestBody Category category){

            Optional<Category> existingCategory = categoryRepository.findById(category.getCategoryId());
            if (!existingCategory.isPresent()) {
                return categoryRepository.save(category);
            } else {
                return existingCategory.get();
            }
    }

    public void deleteCategory(Long id){
        categoryRepository.deleteById(id);
    }
}
