package co.simplon.library.models;

import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "books") // nom de la table
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // autoincrement
    private Long id;

    //non null, longueur maximale de 100 caractères
    @Column(name="title", nullable = false, length = 100)
    private String title;

    @Column(name="description", columnDefinition = "TEXT")
    private String description;

    @Column(name="available", columnDefinition ="BOOLEAN DEFAULT true")
    private Boolean available = true;;

    @ManyToOne // plusieurs Book dans une Category
    @JoinColumn(name = "category_id") // relation avec clé étrangère = categoryId
    private Category category;

    @ManyToMany // nécessite une table de jointure
    @JoinTable(name="author_book",
               joinColumns = @JoinColumn(name="book_id"),
               inverseJoinColumns = @JoinColumn(name = "author_id"))
    private Set<Author> authors = new HashSet<>();;    //1. Set pour éviter les doublons 2. Initialiser la collection directement pour éviter les NullPointerException

    public Book(){}

    public Long getId(){
        return this.id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getTitle(){
        return this.title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public Boolean getAvailable(){
        return this.available;
    }

    public void setAvailable(Boolean available){
        this.available = available;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

}
