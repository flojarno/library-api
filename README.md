# API de Bibliothèque

## Description
Ce projet est une API RESTful pour la gestion des livres d'une bibliothèque. Il comprend les opérations CRUD de base et est construit avec Spring Boot, en utilisant Spring Web, Spring Data JPA et PostgreSQL.

## Prérequis
- PostgreSQL installé et fonctionnant sur votre machine locale.
- Postman pour tester les points de terminaison de l'API.

## Installation

### Cloner le projet
Pour commencer, clonez le dépôt depuis GitLab :
```bash
git clone https://gitlab.com/flojarno/library-api.git
```

### Configurer PostgreSQL
1. Créez une nouvelle base de données nommée `jpalibrary`.
2. Modifiez le `application.properties` dans le projet pour inclure votre utilisateur et mot de passe PostgreSQL :
    ```properties
    spring.datasource.url=jdbc:postgresql://localhost:5432/jpalibrary
    spring.datasource.username=votreUtilisateur
    spring.datasource.password=votreMotDePasse
    ```

### Construire et exécuter
Naviguez dans le répertoire du projet et utilisez Maven pour exécuter le projet :
```bash
cd library-api
./mvnw spring-boot:run
```


## Points de terminaison de l'API à tester avec Postman
- **GET /books** - Récupérer tous les livres.
- **GET /books/{id}** - Récupérer un livre par son ID.
- **GET /books/title/{title}** - Trouver un livre par son titre.
- **POST /books** - Créer un nouveau livre.
    - Exemple de corps JSON :
      ```json
      {
        "title": "Nouveau Livre",
        "description": "Description du nouveau livre",
        "available": true,
        "category": {
            "categoryId": 1,
            "categoryName": "Categorie"
        },
        "authors": [
            {
                "authorId": 1,
                "lastName": "Nom",
                "firstName": "Prenom"
            }
        ]
      }
      ```
- **PUT /books/{id}** - Mettre à jour un livre existant.
- **DELETE /books/{id}** - Supprimer un livre par son ID.
- **GET /authors** - Récupérer tous les auteurs.
- **GET /authors/{id}** - Récupérer un auteur par son ID.
- **GET /authors/{authorId}/books** - Récupérer tous les livres d'un auteur par l'ID de l'auteur.
- **DELETE /authors/{id}** - Supprimer un auteur par son ID.
- **GET /categories** - Récupérer toutes les catégories.
- **GET /categories/{id}** - Récupérer une catégorie par son ID.
- **DELETE /categories/{id}** - Supprimer une catégorie par son ID.
- 
## Test
Utilisez Postman pour tester les points de terminaison de l'API. Assurez-vous que chaque point fonctionne comme prévu en créant, récupérant, mettant à jour et supprimant des livres. Pour plus de conseils sur l'utilisation de Postman, référez-vous à la [documentation officielle de Postman](https://learning.postman.com/docs/getting-started/introduction/).
